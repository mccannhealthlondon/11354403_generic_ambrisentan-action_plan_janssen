// Gulp 

/* ----------------------------------------------
 * Dependancies + config
 */
const gulp 				= require('gulp');
const { watch } 		= require('gulp');
const { series }		= require('gulp');
const sass 				= require('gulp-sass');
const rename 			= require('gulp-rename');
const sourcemaps 		= require('gulp-sourcemaps');
const concat 			= require('gulp-concat');
const browserSync 		= require('browser-sync').create();
const uglify 			= require('gulp-uglify-es').default;
const clc 				= require("cli-color");
const gulpif 			= require('gulp-if');
const cleanCSS 			= require('gulp-clean-css');
const fileinclude 		= require('gulp-file-include');

// Sass config
sass.compiler = require('node-sass');

// File types
// Types
var type = {};
type.img = '.{jpeg,jpg,png,gif,svg,cur,ico}';
type.font = '.{eot,ttf,otf,woff,woff2,svg}';
type.video = '.{mp4,ogv,webm}';
type.audio = '.{wav,mp3}';

/* ----------------------------------------------
 * Log Tasks
 */
// Error logging
function Error(line, filename, msg) {
	console.log(clc.red("== COMPILE ERROR ============================================================================="));
	process.stdout.write(
		clc.columns([
			[clc.red("Line"), clc.red("Filename"), clc.red("Message")],
			[line, filename, msg],
		])
	);
	console.log(clc.red("=============================================================================================="));
}

/* ----------------------------------------------
 * Environment Tasks
 */

/**
 * Browsersync local host
 *
 * @callback            done
 *
 * @returns {Object}
 */
function serve(done) {

	// initialize browsersync
	browserSync.init({
		server: {
			baseDir: "./dist"
		}
	});

	// Watch HTML files 
	watch('src/**/*.html', { events: 'all' }, function (done) {
		compileHTML('src/**/index.html', 'dist/', done);
		browserSync.reload();
	});

	// Watch .scss files 
	watch('src/public/scss/**/*.scss', { events: 'all' }, function (done) {
		compileCss('src/public/scss/app.scss', 'app.min.css', 'dist/public/css/', true, done);
		// browserSync.reload();
	});

	// Watch .js files 
	watch('src/public/js/**/*.js', { events: 'all' }, function (done) {
		compileJs([
			'src/public/js/core.js',
			'src/public/js/**/*.js',
			'src/public/js/app.js',
			'!src/public/js/libs/**/*.js',
		], 'app.min.js', 'dist/public/js/', true, done);
		browserSync.reload();
	});

	done();
};

/* ----------------------------------------------
 * CSS Tasks
 */

/**
 * CSS compiler
 *
 * @param {string}      src			Source Path
 * @param {string}      name        Filename
 * @param {string}      dest        Destination path
 * @param {boolean=}    useSass     Switch for .scss / .css compiler
 * @callback            done
 *
 * @returns {Object}
 */
function compileCss(src, name, dest, useSass, done) {
    useSass = useSass || false;
    gulp.src(src)
        .pipe(sourcemaps.init())
        .pipe(useSass ? sass({ outputStyle: 'compressed' }).on('error', sass.logError) : cleanCSS())
        .pipe(concat(name))
        .pipe(sourcemaps.write())
        .pipe(rename(name))
        .pipe(gulp.dest(dest))
        .pipe(browserSync.stream());
    done();
}

/* ----------------------------------------------
 * Javascript tasks
 */
/**
 * JS compiler
 *
 * @param {string}      src			Source Path
 * @param {string}      name        Filename
 * @param {string}      dest        Destination path
 * @param {boolean=}    sourcemap   Sourcemap switch
 * @callback            done
 *
 * @returns {Object}
 */
function compileJs(src, name, dest, sourcemap, done) {
	gulp.src(src)
		.pipe(gulpif(sourcemap, sourcemaps.init()))
		.pipe(uglify().on('error', function (e) {
			// var msg = 'ERROR: ' + e.message + ' on line ' + e.line + ' of ' + e.filename;
			Error(e.line, e.filename, e.message);
		}))
		.pipe(concat(name))
		.pipe(gulpif(sourcemap, sourcemaps.write()))
		.pipe(rename(name))
		.pipe(gulp.dest(dest))
		.pipe(browserSync.stream());
	done();
}

/* ----------------------------------------------
 * HTML tasks
 */
/**
 * Copy HTML files
 *
 * @param {string}      src			Source Path
 * @param {string}      dest        Destination path
 * @callback            done
 *
 * @returns {Object}
 */
function compileHTML(src, dest, done) {
	gulp.src(src)
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file',
			indent: true
		}))
		.pipe(gulp.dest(dest));
	done();
}

/* ----------------------------------------------
 * Asset tasks
 */
/**
 * Copy files
 *
 * @param {string}      src			Source Path
 * @param {string}      dest        Destination path
 * @callback            done
 *
 * @returns {Object}
 */
function copy(src, dest, done) {
	gulp.src(src)
		.pipe(gulp.dest(dest));
	done();
}

/* ----------------------------------------------
 * Reference tasks
 */

/**
 * CSS compiler task
 * 
 * @module sass
 */
exports.sass = function(done) {
    // Compile Sass
    compileCss('src/public/scss/app.scss', 'app.min.css', 'dist/public/css/', true, done);
};

/**
 * CSS lib compiler task
 * 
 * @module csslibs
 */
exports.csslibs = function (done) {
	// Compile CSS libs
	compileCss('src/public/css/**/*.css', 'libs.min.css', 'dist/public/css/', false, done);
};

/**
 * JS compiler task
 * 
 * @module js
 */
exports.js = function (done) {
	// Compile JS
	compileJs([
		'src/public/js/core.js',
		'src/public/js/**/*.js',
		'src/public/js/app.js',
		'!src/public/js/libs/**/*.js',
	], 'app.min.js', 'dist/public/js/', true, done);
};

/**
 * JS libraries compiler task
 * 
 * @module jslibs
 */
exports.jslibs = function (done) {
	// Compile JS
	compileJs('src/public/js/libs/**/*.js', 'lib.min.js', 'dist/public/js/', false, done);
};

/**
 * HTML task
 * 
 * @module html
 */
exports.html = function (done) {
	// Copy HTML
	compileHTML('src/**/index.html', 'dist/', done);
};

/**
 * Assets task
 * 
 * @module assets
 */
exports.assets = function (done) {
	// Copy Images
	copy('src/public/images/**/*' + type.img, 'dist/public/images/', done);
	// Copy Fonts
	copy('src/public/fonts/**/*' + type.font, 'dist/public/fonts/', done);
	copy('src/public/fonts2/**/*' + type.font, 'dist/public/fonts/', done);
	// Copy icons
	copy('src/public/icons/**/*' + type.img, 'dist/public/icons/', done);
	// Copy JSON
	copy('src/public/json/**/*.json', 'dist/public/json/', done);
};

/**
 * Default task
 * 
 * @module serve
 */
exports.default = series(serve);