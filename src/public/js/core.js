"use strict";

(function () {
    var testElement = document.querySelector(".container-fluid");
    var isWKWebView = false;

    if (navigator.platform.substr(0, 2) === "iP") {
        // iOS detected
        if (window.webkit && window.webkit.messageHandlers) {
            isWKWebView = true; //   testElement.innerHTML = "WK WebView";
        } else {// testElement.innerHTML = "UI WebView";
        }
    }
})();

/* 
 * DomReady
 * https://stackoverflow.com/a/25560381/737794
 */
(function (exports, d) {
    function domReady(fn, context) {

        function onReady(event) {
            d.removeEventListener("DOMContentLoaded", onReady);
            fn.call(context || exports, event);
        }

        function onReadyIe(event) {
            if (d.readyState === "complete") {
                d.detachEvent("onreadystatechange", onReadyIe);
                fn.call(context || exports, event);
            }
        }

        d.addEventListener && d.addEventListener("DOMContentLoaded", onReady) ||
            d.attachEvent && d.attachEvent("onreadystatechange", onReadyIe);
    }

    exports.domReady = domReady;
})(window, document);

/* 
 * Namespace
 */
const _Pres = {};

/* 
 * Check page
 */
_Pres.checkPage = function() {
    return document.body.id;
}

/* 
 * Pages
 */
_Pres.Pages = {};

/* 
 * Components
 */
_Pres.Components = {};