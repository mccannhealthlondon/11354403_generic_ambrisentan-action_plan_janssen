"use strict";

/* 
 * Init App
 */
function init(event) {
    let page = _Pres.checkPage();
    if (typeof _Pres.Pages[page] !== 'undefined') {
        _Pres.Pages[page]();
    }
}

domReady(init);