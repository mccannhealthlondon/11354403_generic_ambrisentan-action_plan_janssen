/* 
 * Page 2 actions
 */
_Pres.Pages.page02 = function() {
    // Get the links
    let content = document.querySelector('.content');
    let nav = content.querySelector('nav');
    let links = nav.getElementsByTagName('a');
    for (let i = 0; i < links.length; i++) {
        let link = links[i];
        addClick(link)
    }

    function addClick(elem) {
        elem.addEventListener('click', function(e) {
            e.preventDefault();
            let link = elem.dataset.link;
            window.location = link;
            
            /*
             * Clicktag goes here
             */
            
        }, false)
    }
};