/* 
 * Template page actions
 */
_Pres.Pages.page_template = function () {
    let slideshow = document.querySelector('.slideshow');
    if (typeof slideshow !== 'undefined') {
        let active = "01";
        let slides = document.querySelector('.slides');
        let nav = document.querySelector('nav');
        let links = nav.getElementsByTagName('a');
        slides.classList.add("slide-" + active);
        for (let i = 0; i < links.length; i++) {
            let link = links[i];
            addClick(link);
        }
        function addClick(elem) {
            elem.addEventListener('click', function (e) {
                e.preventDefault();
                slides.classList.remove("slide-" + active);
                slides.classList.add('slide-' + elem.dataset.slide);
                active = elem.dataset.slide;
                setActive(active)
            }, false)
        }
        function setActive(num) {
            let ul = nav.querySelector('ul');
            for (let i = 0; i < ul.children.length; i++) {
                const item = ul.children[i].querySelector('a');
                if (item.dataset.slide == num) {
                    item.className = "active";
                } else {
                    item.className = "";
                }
            }
            
        }
    }
};